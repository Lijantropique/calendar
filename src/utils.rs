use std::io::{self,Write};
use std::str::FromStr;
use time::{Date, Month};

pub const DAYS:[&str; 7] = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"];
pub const MONTHS:[&str; 12] = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "Decemeber"];

pub fn print_banner(message: &str){
    println!("----------------------
{message}
By Al Sweigart al@inventwithpython.com
(rust-ed 🦀 by lij)
----------------------\n");
}

pub fn get_input(message: &str) -> String {
    println!("{message}");
    print!("> ");
    io::stdout().flush().unwrap();
    let mut guess = String::new();
    io::stdin()
        .read_line(&mut guess)
        .expect("Failed to read line");
    guess.trim().to_string()
}

pub fn validate(message:&str, min:i32, max:i32) -> i32{
    let mut tmp; 
    loop {
        let raw = get_input(message);
        tmp = match raw.parse() {
            Ok(n) => n,
            Err(_) => {
                println!("... please enter a number! ...");
                min-1
            },
        };
        if tmp >min && tmp < max  {break}
    }
    tmp
}

pub fn get_calendar_for(year:i32, month:i32){
    let n = usize::try_from(month-1).unwrap();
    let mut cal_text = String::new(); // calText will contain the string of our calendar.

    // Write headers (Month-Year) & days of the week
    let mut tmp = format!("{}{}-{}\n"," ".repeat(34), MONTHS[n], year);
    for day in DAYS {
        tmp += &format!("....{day}....");
    }
    cal_text += &(tmp + ".\n");
    let week_separator = "+----------".repeat(7) + "+";
    let empty_separator = "|          ".repeat(7) + "|";
    cal_text += &(week_separator.clone() + "\n");
    
    let month = Month::from_str(MONTHS[n]).unwrap();
    let mut current_date = Date::from_calendar_date(year, month, 1).unwrap();
    let mut weekday = current_date.weekday();
    // number_days_from_monday ->  equivalent to python datetime.time()                         
    while weekday.number_days_from_monday() != 6 {
        current_date = current_date.previous_day().unwrap();
        weekday = current_date.weekday();
    }

    // iterate over all the weeks in the month
    loop {
        // add numbers to the top left
        let mut day_number_row = String::new();
        for _ in 0..7 {
            let day_number_label = format!("{:2}", current_date.day());
            day_number_row += &format!("|{}{}", day_number_label, " ".repeat(8));
            current_date = current_date.next_day().unwrap();
        }
        day_number_row.push('|');
        cal_text += &(day_number_row + "\n");
        
        // add the remaining empty rows
        for _ in 0..3 {
            cal_text += &(empty_separator.clone() + "\n");
        }
        cal_text += &(week_separator.clone() + "\n");

        // verify if we complete all the weeks of the month
        if current_date.month() != month{
            break;
        }
    }


    println!("{}", cal_text);
}