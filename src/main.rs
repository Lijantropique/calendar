mod utils;

fn main() {
    utils::print_banner("📅 Calendar Maker 📅");
    let year = utils::validate("Enter the year for the calendar:",0,10000);
    let month = utils::validate("Enter the month for the calendar, 1-12:",0,13);
    utils::get_calendar_for(year, month);
}
